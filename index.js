var tools = require('./lib/tools');
var mongoRepo = require('./lib/mongoRepository');
var mongoModel = require('./lib/mongoModel');
var logger = require('./lib/logger');
var promises = require('./lib/promises');
var promise = require('./lib/promise');
var prototypes = require('./lib/prototypes');

module.exports = {
	tools            : tools,
	mongo_repository : mongoRepo,
	mongo_model      : mongoModel,
	logger           : logger,
	promises         : promises,
	promise          : promise,
	check            : function () {
		return 'Mongo Tools'
	}
};